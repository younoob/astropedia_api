<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Str;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('posts', 'PostController@index');
$router->post('posts', 'PostController@store');
$router->get('posts/image/{name}', 'PostController@getImage');
$router->get('posts/{id}', 'PostController@show');
$router->post('posts/update/{id}', 'PostController@update');
$router->delete('posts/delete/{id}', 'PostController@destroy');

$router->get('key', function() {
    $key = Str::random(32);
    return $key;
});
