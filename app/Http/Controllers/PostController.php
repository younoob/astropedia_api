<?php

namespace App\Http\Controllers;

use App\Models\Post;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return response()->json([
            'success' => true,
            'message' => 'All availabe data',
            'data' =>  $posts
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'All column must reuired!',
                'data' => $validator->errors()
            ], 401);
        }else {
            $image = Str::random(15);
            $request->file('image')->move(storage_path('image'), $image);

            $post = Post::create([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'image' => $image
            ]);

            if ($post) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data posted',
                    'data' => $post
                ], 201);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => "Can't posting data",
                ],400);
            }
        }

    }

    public function getImage($name)
    {
        $post_path = storage_path('image') . '/' . $name;

        if (file_exists($post_path)) {
            $file = file_get_contents($post_path);
            return response($file, 200)->header('Content-Type', 'image/jpeg');
        }

        $response['success'] = false;
        $response['message'] = 'Image not found';
        return $response;
    }

    public function show($id)
    {
        $post = Post::find($id);

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Detail data by id '.$post->id,
                'data' => $post
            ], 200);
        }else {
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'All column must required',
                'data' => $validator->errors()
            ], 401);
        }else {

            $post_image = Post::find($id);

            $image = Str::random(15);
            $request->file('image')->move(storage_path('image'), $image);

            if ($post_image) {
                $current_post_path = storage_path('image') . '/' . $post_image->image;

                if (file_exists($current_post_path)) {
                    unlink($current_post_path);
                }
            }

            $post_image->update([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'image' => $image
            ]);

            if ($post_image) {
                return response()->json([
                    'success' => true,
                    'message' => 'Post updated',
                    'data' => $post_image
                ], 201);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => "Can't updating post by id ".$post_image->id
                ], 400);
            }
        }
    }

    public function destroy($id)
    {

        $post = Post::whereId($id)->first();

        // $image_path = $post->image;

        // if (Storage::exists(storage_path($image_path))) {
        //     Storage::delete(storage_path($image_path));
        // }

        if ($post->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Data deleted',
            ], 200);
        }else {
            return response()->json([
                'success' => true,
                'message' => "Can't deleting data"
            ], 400);
        }
    }
}
